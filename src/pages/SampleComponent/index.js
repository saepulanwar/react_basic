import React, {Component} from 'react';
import {View, Text, TextInput, Image} from 'react-native';
const SampleComponent = () => {
    return (
      <View>
        <View style={{width: 80, height: 80, backgroundColor:'#0abde3'}}/>
        <Text>1</Text>
        <Home />
        <Text>2</Text>
        <Photo />
        <TextInput style={{borderWidth:1}}/>
        <BoxGreen />
        <Profile />
      </View>
    );
  }
  
  const Home = () => {
    return <Text>home</Text>;
  };
  
  const Photo = () => {
    return (
    <Image 
    source={{uri:'https://images.unsplash.com/photo-1538895490524-0ded232a96d8?ixlib=rb-1.2.1&auto=format&fit=crop&w=844&q=80'}}
    style={{width:80, height:80}}
    />
    );
  };
  
  class BoxGreen extends Component {
    render (){
      return <Text>class component</Text>
    }
  }
  
  class Profile extends Component {
    render() {
      return (
      <View>
        <Image 
          source={{uri:'https://images.unsplash.com/photo-1519750292352-c9fc17322ed7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=932&q=80'}}
          style={{width:100, height:100, borderRadius:100}}
        />
        <Text style={{textAlign:'center'}}>Profile</Text>
      </View>
      )
    }
  }

  export default SampleComponent;