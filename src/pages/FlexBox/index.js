import React, {Component, useEffect, useState} from 'react';
import {Text, View, Image} from 'react-native';
// class FlexBox extends Component {
//     // siklus hidup class component
//     constructor(props){
//         super(props);
//         console.log('=> constructor');
//         this.state={
//             Subscribers: 100,
//         };
//     }

//     componentDidMount() {
//         console.log('=> component didmount');
//         setTimeout(()  => {
//             this.setState({
//                 Subscribers: 400,
//             })
//         }, 2000)

//     }
//     componentDidUpdate(){
//         console.log('=> component didupdate');
//     }

//     componentWillUnmount(){
//         console.log('=> component willunmount');
//     }
//     render (){
//         // console.log('hello');
//         return (
//         <View>
//             <View 
//             style={{
//                 flexDirection: 'row', 
//                 backgroundColor:'grey', 
//                 alignItems:'flex-end',
//                 justifyContent:'space-between'
//             }}>
//                 {/* <Text>Materi flex box</Text> */}
//                 {/* <View style={{backgroundColor:'red', width:50, height:50}}/>
//                 <View style={{backgroundColor:'blue', flex:1, height:100}}/>
//                 <View style={{backgroundColor:'green', flex:1, height:150}}/>
//                 <View style={{backgroundColor:'yellow', flex:1, height:200}}/> */}

//                 <View style={{backgroundColor:'red', width:50, height:50}}/>
//                 <View style={{backgroundColor:'blue', width:50, height:50}}/>
//                 <View style={{backgroundColor:'green', width:50, height:50}}/>
//                 <View style={{backgroundColor:'yellow', width:50, height:50}}/>
//             </View>
//             <View
//             style={{
//                 flexDirection:'row',
//                 justifyContent: 'space-around'
//             }}>
//                 <Text>Home</Text>
//                 <Text>View</Text>
//                 <Text>Profile</Text>
//                 <Text>Dashboard</Text>
//                 <Text>Channel</Text>
//             </View>
//             <View style={{flexDirection:'row', alignItems:'center', marginTop:20}}>
//                 <Image
//                 source={{
//                     uri:'https://images.unsplash.com/photo-1484800089236-7ae8f5dffc8e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80'
//                 }}
//                 style={{width:100, height: 100, borderRadius:100, marginRight:14
//                 }} />
//                 <View>
//                     <Text style={{fontWeight:'bold', fontSize:24}}>Saepul Anwar</Text>
//                     <Text>{this.state.Subscribers} Subscribers</Text>
//                 </View>
//             </View>
//         </View>
//         )
//     }
// } 

const FlexBox = () => {
    // siklus hidup class component
    const [subscriber, setSubscriber] = useState(200)
        useEffect(()=> {
            console.log('did mount');
            setTimeout(()=>{
                setSubscriber(400);
            }, 2000)
            return () => {
                console.log('did update');
            };
        }, [subscriber]);

        // useEffect(()=> {
        //     console.log('did update');
        //     setTimeout(()=>{
        //         setSubscriber(400);
        //     }, 2000)
        // }, [subscriber]);

        return (
        <View>
            <View 
            style={{
                flexDirection: 'row', 
                backgroundColor:'grey', 
                alignItems:'flex-end',
                justifyContent:'space-between'
            }}>
                {/* <Text>Materi flex box</Text> */}
                {/* <View style={{backgroundColor:'red', width:50, height:50}}/>
                <View style={{backgroundColor:'blue', flex:1, height:100}}/>
                <View style={{backgroundColor:'green', flex:1, height:150}}/>
                <View style={{backgroundColor:'yellow', flex:1, height:200}}/> */}

                <View style={{backgroundColor:'red', width:50, height:50}}/>
                <View style={{backgroundColor:'blue', width:50, height:50}}/>
                <View style={{backgroundColor:'green', width:50, height:50}}/>
                <View style={{backgroundColor:'yellow', width:50, height:50}}/>
            </View>
            <View
            style={{
                flexDirection:'row',
                justifyContent: 'space-around'
            }}>
                <Text>Home</Text>
                <Text>View</Text>
                <Text>Profile</Text>
                <Text>Dashboard</Text>
                <Text>Channel</Text>
            </View>
            <View style={{flexDirection:'row', alignItems:'center', marginTop:20}}>
                <Image
                source={{
                    uri:'https://images.unsplash.com/photo-1484800089236-7ae8f5dffc8e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80'
                }}
                style={{width:100, height: 100, borderRadius:100, marginRight:14
                }} />
                <View>
                    <Text style={{fontWeight:'bold', fontSize:24}}>Saepul Anwar</Text>
                    <Text>{subscriber} Subscribers</Text>
                </View>
            </View>
        </View>
        )
    } 

export default FlexBox; 