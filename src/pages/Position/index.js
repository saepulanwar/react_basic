import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const Position = () => {
    return (
        <View style={styles.wrapper}>
            <View style={styles.cardWrapper}>
                <Image source={{uri:'https://www.pinclipart.com/picdir/middle/543-5433399_online-shopping-shopping-cart-logo-icon-shop-online.png'}} style={styles.iconCart}/>
                <Text style={styles.notif}>99</Text>
            </View>
            <Text style={styles.text}>Keranjang Belanja Anda</Text>
        </View>
    );
};

export default Position;

const styles = StyleSheet.create({
    iconCart: {width: 50, height:50},
    wrapper:{padding:20, alignItems:'center'},
    cardWrapper:{
        borderWidth:1, 
        borderColor:'#439801',
        width:93,
        height: 93,
        borderRadius: 93/2,
        alignItems: 'center',
        justifyContent:'center',
        position:'relative'
    },
    text: {fontSize:12, color:'#777777', fontWeight: '700', marginTop:10},
    notif: {
        fontSize:12,
        color: 'white',
        backgroundColor:'#6fcf97',
        padding:4,
        borderRadius:25,
        width:24,
        height:24,
        position:'absolute',
        top:0,
        right:0
    }
});
