import React from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'

const Story = props=>{
    return(
        <View>
            <Image 
                source={{uri:props.cover}} 
                style={{width:50, height:50, borderRadius:25}}/>
            <Text style={{maxWidth:50, textAlign: 'center', marginRight:20}}>{props.title}</Text>
        </View>
    )
}
const PropsDinamis = () => {
    return(
        <View>
            <Text> Materi component props dinamis </Text>
            <ScrollView horizontal>
                <View style={{flexDirection:'row'}}>
                    <Story title="Youtube Channel" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Kelas Online" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Kabayan Coding" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Youtube aja" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Kabayan Coding" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Youtube aja" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Kabayan Coding" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                    <Story title="Youtube aja" cover="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </View>
            </ScrollView>
            
        </View>
    )
}
export default PropsDinamis;
const styles = StyleSheet.create({})

