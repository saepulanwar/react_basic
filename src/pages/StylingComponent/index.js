import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
// import scooter from '../../assets/images/scooter.jpg';
const StylingComponent = () => {
    return (
      <View>
        <Text style={styles.text}>styling</Text>
        <View style={{
          width:100, 
          height:100, 
          backgroundColor:'#0abde3', 
          borderWidth:2, 
          borderColor:'#5f27cd',
          marginTop:20
          }}
        />
        <View style={{padding:12, backgroundColor:'#f2f2f2', width:212}}>
          <Image 
            source={{uri:'https://images.unsplash.com/photo-1591122519484-70428711810d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80'}}
            style={{width:188, height: 107, borderRadius:8}}
          />
          <Text style={{fontSize:14, fontWeight:'bold', marginTop:16}}>Scooter Classic 2000</Text>
          <Text style={{fontSize:14, fontWeight:'bold', marginTop:16}}>Rp. 25.000.000</Text>
          <Text style={{fontSize:12, fontWeight:'300', marginTop:16}}>Jakarta</Text>
          <View style={{paddingVertical:6, marginTop:20, borderRadius:25, backgroundColor:'green'}}>
            <Text style={{fontSize:14, fontWeight:'600', color:'white', textAlign:'center'}}>Beli</Text>
          </View>
        </View>
      </View>
    )
  };
  const styles = StyleSheet.create({
    text:{
      fontSize:18,
      fontWeight:'bold',
      color:'green',
      marginLeft:20
    }
  })

  export default StylingComponent;