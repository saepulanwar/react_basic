import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

const Product = (props)=>{
    return (
        <View style={styles.wrapper}>
          <Image 
            source={{uri:'https://images.unsplash.com/photo-1591122519484-70428711810d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80'}}
            style={styles.imageProduct}
          />
          <Text style={styles.productName}>Scooter Classic 2000</Text>
          <Text style={styles.productName}>Rp. 25.000.000</Text>
          <Text style={styles.textLocation}>Jakarta</Text>
          <TouchableOpacity onPress={props.onButtonPress}>
            <View style={styles.buttonWrapper}>
                <Text style={styles.buttonText}>Beli</Text>
            </View>
          </TouchableOpacity>

        </View>
    )
}
export default Product;

const styles = StyleSheet.create({
    wrapper :{
        padding:12, 
        backgroundColor:'#f2f2f2', 
        width:212
    },
    imageProduct: {
        width:188, 
        height: 107, 
        borderRadius:8
    },
    productName: {
        fontSize:14, 
        fontWeight:'bold', 
        marginTop:16
    },
    textLocation: {
        fontSize:12, 
        fontWeight:'300', 
        marginTop:16
    },
    buttonWrapper: {
        paddingVertical:6, 
        marginTop:20, 
        borderRadius:25, 
        backgroundColor:'green'
    },
    buttonText: {
        fontSize:14, 
        fontWeight:'600', 
        color:'white', 
        textAlign:'center'
    }

})
